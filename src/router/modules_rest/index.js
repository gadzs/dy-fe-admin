import Layout from '@/layout/index'

// constantRoutes 从 @router/index.js 复制，自定义时修改这里
export const constantRoutes = [
  {
    path: '/redirect',
    component: Layout,
    hidden: true,
    children: [
      {
        path: '/redirect/:path(.*)',
        component: () => import('@/views/redirect/index')
      }
    ]
  },
  {
    path: '/login',
    component: () => import('@/views/rest/unified/login/index'),
    hidden: true
  },
  {
    path: '/404',
    component: () => import('@/views/error-page/404'),
    hidden: true
  },
  {
    path: '/401',
    component: () => import('@/views/error-page/401'),
    hidden: true
  },
  {
    path: '/',
    redirect: '/unified/dashboard',
    hidden: true
  },
  {
    path: '/unified/dashboard',
    name: 'Dashboard',
    component: () => import('@/views/rest/unified/dashboard/index'),
    meta: { title: '首页', icon: 'dashboard', affix: false },
    hidden: true
  },
  {
    path: '/platform',
    component: Layout,
    redirect: '/platform/dashboard',
    children: [
      {
        path: '/platform/dashboard',
        component: () => import('@/views/rest/platform/dashboard/index'),
        name: 'PlatformDashboard',
        meta: { title: '首页', icon: 'dashboard', affix: false },
        hidden: true
      }
    ],
    hidden: true
  },
  {
    path: '/cms',
    component: Layout,
    redirect: '/cms/dashboard',
    children: [
      {
        path: '/cms/dashboard',
        component: () => import('@/views/rest/cms/dashboard/index'),
        name: 'CmsDashboard',
        meta: { title: '首页', icon: 'dashboard', affix: false },
        hidden: true
      }
    ],
    hidden: true
  },
  {
    path: '/index', // 门户网站-首页
    name: '/index',
    component: () => import('@/views/rest/portal/index'),
    meta: {},
    hidden: true
  },
  {
    path: '/news', // 门户网站-新闻列表
    name: '/news',
    component: () => import('@/views/rest/portal/news'),
    meta: {},
    hidden: true
  },
  {
    path: '/appearance', // 门户网站-家政风采
    name: '/appearance',
    component: () => import('@/views/rest/portal/appearance'),
    meta: {},
    hidden: true
  },
  {
    path: '/regulation', // 门户网站-政策法规
    name: '/regulation',
    component: () => import('@/views/rest/portal/regulation'),
    meta: {},
    hidden: true
  },
  {
    path: '/newsDetail', // 门户网站-新闻详情
    name: '/newsDetail',
    component: () => import('@/views/rest/portal/news-detail.vue'),
    meta: {},
    hidden: true
  },
  {
    path: '/communication', // 门户网站-交流互动
    name: '/communication',
    component: () => import('@/views/rest/portal/communication.vue'),
    meta: {},
    hidden: true
  },
  // {
  //   path: '/search', // 门户网站-服务查询
  //   name: '/search',
  //   component: () => import('@/views/rest/portal/service-search.vue'),
  //   meta: {},
  //   hidden: true
  // },
  {
    path: '/knowledge', // 门户网站-知识库
    name: '/knowledge',
    component: () => import('@/views/rest/portal/knowledge-list.vue'),
    meta: {},
    hidden: true
  },
  {
    path: '/business', // 门户网站-商务信息
    name: '/business',
    component: () => import('@/views/rest/portal/business'),
    meta: {},
    hidden: true
  }
]

export function buildAsyncRoutes(routeTable) {
  return [
    // 自定义异步扩展路由在这里定义
    ...routeTable,
    // 404 page must be placed at the end !!!
    { path: '*', redirect: '/', hidden: true }
  ]
}
