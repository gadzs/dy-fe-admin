import axios from 'axios'
import store from '@/store'
import { getToken } from '@/utils/auth'
import router from '@/router'
import { Message, MessageBox } from 'element-ui'

// create an axios instance
const service = axios.create({
  baseURL: process.env.VUE_APP_BASE_API, // url = base url + request url
  withCredentials: true, // send cookies when cross-domain requests
  timeout: 10000 // request timeout
})

// request interceptor
service.interceptors.request.use(
  (config) => {
    // do something before request is sent
    if (store.getters.token) {
      // let each request carry token
      // ['X-Token'] is a custom headers key
      // please modify it according to the actual situation
      config.headers['X-Token'] = getToken()
    }
    return config
  },
  (error) => {
    // do something with request error
    console.log(error) // for debug
    return Promise.reject(error)
  }
)

// response interceptor
service.interceptors.response.use(
  /**
   * If you want to get http information such as headers or status
   * Please return  response => response
   */

  /**
   * Determine the request status by custom code
   * Here is just an example
   * You can also judge the status by HTTP Status Code
   */
  (response) => {
    if (response.headers['content-type'] === 'application/vnd.ms-excel;charset=utf-8') {
      return { code: 0, data: response.data, success: true }
    }
    const res = response.data

    res.success = res.code === 0

    // if the custom code is not 20000, it is judged as an error.
    if (res.code !== 0) {
      Message({
        message: res.message || 'Error',
        type: 'error',
        duration: 5 * 1000
      })

      return Promise.reject(new Error(res || 'Error'))
    }
    return res
  },
  (error) => {
    const response = error.response
    const res = response.data

    if (response.status === 401) {
      if (res.code === 40100) {
        if (router.currentRoute.path !== '/') {
          // to re-login
          MessageBox.confirm('您的登录已退出，是否重新登录？取消则留在当前页面。', '确认重新登录', {
            confirmButtonText: '重新登录',
            cancelButtonText: '取消',
            type: 'warning'
          }).then(() => {
            store.dispatch('user/resetToken').then(() => {
              location.reload()
            })
          })
        }
        return Promise.reject(error)
      }
    }

    let message = error.message

    if (response && res) {
      message = res.message
    }

    // console.log(`err${error}`) // for debug
    Message({
      message: message,
      type: 'error',
      duration: 5 * 1000
    })
    return Promise.reject(error)
  }
)

export default service
