// 企业类型
export const DICT_CONFIG_KEY_COMPANY_TYPE = 'companyType'
// 企业人员规模
export const DICT_CONFIG_KEY_EMPLOYEE_SIZE = 'employeeSize'
// 家政企业类型
export const DICT_CONFIG_KEY_COMPANY_HOUSEKEEPING_TYPE = 'housekeepingCompanyType'
// 家政员类型
export const DICT_CONFIG_KEY_STAFF_TYPE = 'staffType'
// 活动类型
export const DICT_CONFIG_KEY_ACTIVITY_TYPE = 'activityType'
// 知识类型
export const DICT_CONFIG_KEY_KNOWLEDGE_TYPE = 'knowledgeType'
// 默认省编码
export const DEFALT_PROVINCE_CODE = 140000
// 默认市编码
export const DEFALT_CITY_CODE = 140800

export const ADMIN_ROLE = 'admin'

export const PLATFORM_ROLE = 'platform'

export const COMPANY_ROLE = 'company'
