function listToMap(list) {
  const map = {}

  list.forEach((item) => {
    map[item.value] = item.name
  })

  return map
}

const appList = [
  {
    value: 'platform',
    name: '平台管理系统'
  },
  {
    value: 'cms',
    name: '公共服务系统'
  }
]

export const appMap = listToMap(appList)

export const yesNoStatusList = [
  {
    value: 1,
    name: '是'
  },
  {
    value: 0,
    name: '否'
  }
]
export const yesNoStatusMap = listToMap(yesNoStatusList)

export const enableStatusList = [
  {
    value: 1,
    name: '启用'
  },
  {
    value: 0,
    name: '禁用'
  }
]
export const enableStatusMap = listToMap(enableStatusList)

export const shopStatusList = [
  {
    value: 0,
    name: '注册待审核'
  },
  {
    value: 1,
    name: '已审核'
  },
  {
    value: 2,
    name: '审核未通过'
  },
  {
    value: 3,
    name: '申请注销'
  },
  {
    value: 4,
    name: '已注销'
  }
]
export const shopStatusMap = listToMap(shopStatusList)
export const genderList = [
  {
    value: 1,
    name: '男'
  },
  {
    value: 2,
    name: '女'
  }
]
export const genderMap = listToMap(genderList)

export const areaList = [
  {
    value: 140802,
    name: '盐湖区'
  },
  {
    value: 140821,
    name: '临猗县'
  },
  {
    value: 140822,
    name: '万荣县'
  },
  {
    value: 140823,
    name: '闻喜县'
  },
  {
    value: 140824,
    name: '稷山县'
  },
  {
    value: 140825,
    name: '新绛县'
  },
  {
    value: 140826,
    name: '绛县'
  },
  {
    value: 140827,
    name: '垣曲县'
  },
  {
    value: 140828,
    name: '夏县'
  },
  {
    value: 140829,
    name: '平陆县'
  },
  {
    value: 140830,
    name: '芮城县'
  },
  {
    value: 140881,
    name: '永济市'
  },
  {
    value: 140882,
    name: '河津市'
  }
]
export const areaMap = listToMap(areaList)

export const usedStatusList = [
  {
    value: 0,
    name: '未使用'
  },
  {
    value: 10,
    name: '已分配'
  },
  {
    value: 99,
    name: '作废'
  }
]
export const usedStatusMap = listToMap(usedStatusList)

export const activityScopeList = [
  {
    value: 20,
    name: '公司'
  },
  {
    value: 30,
    name: '家政员'
  },
  {
    value: 40,
    name: '消费者'
  }
]
export const activityScopeMap = listToMap(activityScopeList)

export const entityObjectTypeList = [
  { value: 0, name: '默认' },

  { value: 20, name: '分类' },

  { value: 40, name: '店铺' }
]

export const entityObjectTypeMap = listToMap(entityObjectTypeList)

export const activityAuditStatusList = [
  { value: 0, name: '待审核' },
  { value: 10, name: '审核通过' },
  { value: 99, name: '审核未通过' }
]

export const activityAuditStatusMap = listToMap(activityAuditStatusList)

export const starLevelList = [
  {
    value: 0,
    name: '非星社区'
  },
  {
    value: 3,
    name: '三星社区'
  },
  {
    value: 4,
    name: '四星社区'
  },
  {
    value: 5,
    name: '五星社区'
  }
]
export const starLevelMap = listToMap(starLevelList)
