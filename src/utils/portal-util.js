const colorWhite = '#ffffff'
const startVal = 0
const defaultNum = 0
const numSeparator = ''
const numSeparatorDou = ','
const itemUnit = 'px'
const itemPercentUnit = '%'
const dataDuration = 6000
const infoCategoryCode = 'INFO'

// copyJSON
const copyJSON = function(option) {
  return JSON.parse(JSON.stringify(option))
}

const gotoPageOpen = function(obj, path, params) {
  const routeUrl = obj.$router.resolve({
    path: path,
    query: params
  })
  window.open(routeUrl.href, '_blank')
}

// 跳转route
const gotoPage = function(obj, name, params) {
  if (name === '/') {
    const routeUrl = obj.$router.resolve({
      path: name,
      query: {}
    })
    window.open(routeUrl.href, '_blank')
  } else {
    obj.$router.push({ name: name, params: params })
  }
}

const protalTabPages = [
  { name: '首页', path: '/index' },
  { name: '新闻资讯', path: '/news' },
  { name: '政策法规', path: '/regulation' },
  { name: '商务信息', path: '/business' },
  { name: '便民风采', path: '/appearance' },
  { name: '互动交流', path: '/communication' }
]

const interactionType = [
  { id: 1, name: '专业咨询' },
  { id: 2, name: '公众留言' },
  { id: 3, name: '常见问题' },
  { id: 4, name: '网上投诉' }
]

const gotoPageIndex = function(obj, index, params) {
  gotoPage(obj, protalTabPages[index].path, params)
}

export default {
  colorWhite,
  startVal,
  defaultNum,
  itemUnit,
  itemPercentUnit,
  numSeparator,
  numSeparatorDou,
  dataDuration,
  protalTabPages,
  copyJSON,
  gotoPage,
  gotoPageIndex,
  infoCategoryCode,
  interactionType,
  gotoPageOpen
}
