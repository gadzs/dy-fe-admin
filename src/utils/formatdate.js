export function formatDate(val) {
  return formatDates(val, 'yyyy-MM-dd')
}

export function formatDateTime(val) {
  return formatDates(val, 'yyyy-MM-dd hh:mm:ss')
}

export function formatDates(dat, fmt) {
  fmt = fmt || 'yyyy-MM-dd hh:mm:ss'
  const date = dat ? new Date(dat) : new Date()
  let ret
  const weekArr = ['日', '一', '二', '三', '四', '五', '六']
  const opt = {
    'y+': date.getFullYear().toString(), // 年
    'M+': (date.getMonth() + 1).toString(), // 月
    'd+': date.getDate().toString(), // 日
    'h+': date.getHours().toString(), // 时
    'm+': date.getMinutes().toString(), // 分
    's+': date.getSeconds().toString(), // 秒
    'W+': date.getDay(), // 星期
    'Q+': Math.ceil((date.getMonth() + 1) / 3) // 季度   // 有其他格式化字符需求可以继续添加，必须转化成字符串
  }
  for (const k in opt) {
    ret = new RegExp('(' + k + ')').exec(fmt)
    if (ret) {
      fmt = fmt.replace(
        ret[1],
        ret[1].length === 1
          ? k === 'W+' || k === 'Q+'
            ? weekArr[opt[k]]
            : opt[k]
          : opt[k].padStart(ret[1].length, '0')
      )
    }
  }
  return fmt
}

/* * 时间格式化工具
 * 把Long类型的1527672756454日期还原yyyy-MM-dd 00:00:00格式日期
 */
export function datetimeFormat(longTypeDate) {
  var dateTypeDate = ''
  var date = new Date()
  date.setTime(longTypeDate)
  dateTypeDate += date.getFullYear() // 年
  dateTypeDate += '-' + getMonth(date) // 月
  dateTypeDate += '-' + getDay(date) // 日
  dateTypeDate += ' ' + getHours(date) // 时
  dateTypeDate += ':' + getMinutes(date) // 分
  dateTypeDate += ':' + getSeconds(date) // 分
  return dateTypeDate
}

// 返回 01-12 的月份值
function getMonth(date) {
  var month = ''
  month = date.getMonth() + 1 // getMonth()得到的月份是0-11
  if (month < 10) {
    month = '0' + month
  }
  return month
}

// 返回01-30的日期
function getDay(date) {
  var day = ''
  day = date.getDate()
  if (day < 10) {
    day = '0' + day
  }
  return day
}

// 小时
function getHours(date) {
  var hours = ''
  hours = date.getHours()
  if (hours < 10) {
    hours = '0' + hours
  }
  return hours
}

// 分
function getMinutes(date) {
  var minute = ''
  minute = date.getMinutes()
  if (minute < 10) {
    minute = '0' + minute
  }
  return minute
}

// 秒
function getSeconds(date) {
  var second = ''
  second = date.getSeconds()
  if (second < 10) {
    second = '0' + second
  }
  return second
}
