import moment from 'moment'
import store from '@/store'

window.onload = function() {
  window.document.onmousedown = function() {
    return store.dispatch('user/setLastDateTime', moment.now())
  }
}

function checkTimeout() {
  const currentTime = moment.now()		// 更新当前时间
  const lastTime = localStorage.getItem('lastDateTime')
  const lockScreenTime = localStorage.getItem('lockScreenTime')

  if (moment(Number(lastTime)).add(lockScreenTime, 'seconds').isBefore(currentTime)) { // 判断是否超时
    store.dispatch('user/setLockScreen', true).then(() => {
    })
  }
}

export default function() {
  /* 定时器 间隔10秒检测是否长时间未操作页面 */
  window.setInterval(checkTimeout, 10000)
}
