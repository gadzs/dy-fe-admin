const colorWhite = '#ffffff'
const colorBlack = '#000000'
const colorGrey = 'rgba(255, 255, 255, 0.6)'
const startVal = 0
const defaultNum = 0
const numSeparator = ''
const numSeparatorDou = ','
const dataDuration = 6000
const defaultAreaCode = '370500'
const defaultAreaCodeData = 370500
const defaultYhAreaCode = '370500'
const defaultYhAreaCodeData = 370500
const defaultProvinceList = [{ value: 370500, name: '东营市' }]
const defaultAreaEnd = '00'
const defaultAreaName = '东营市'
const defaultSmallAreaName = '东营'
const defaultYhAreaName = '东营市'
const defaultYhSmallAreaName = '东营'
const minClientWidth = 1300
const tipBgColor = 'rgba(1, 97, 210, 0.8)'
const tipTimer = 2500
const chartPushTimer = 5000
const chartDefaultAxisNum = 7
const entImg = []
const itemUnit = 'px'
const itemPercentUnit = '%'
const colorArray = ['RGBA(255,144,26)', '#50C1B9', '#0F46E8', '#AA645A']
const colorArrayLinear = [['RGBA(255,144,26)', 'RGBA(255,232,127)'],
  ['RGBA(90,255,250)', 'RGBA(152,245,245)'],
  ['RGBA(239,53,103)', 'RGBA(251,89,160)'],
  ['RGBA(51,255,152)', 'RGBA(115,255,199)'],
  ['RGBA(49,114,236)', 'RGBA(39,154,250)'],
  ['RGBA(0,221,255)', 'RGBA(0,221,255)'],
  ['RGBA(128,35,184)', 'RGBA(175,100,222)'],
  ['RGBA(240,39,224)', 'RGBA(234,108,224)'],
  ['RGBA(227,152,3)', 'RGBA(227,152,3)'],
  ['RGBA(3,15,227)', 'RGBA(3,15,227)']]
const colorArrayLinear2 = [
  ['RGBA(31,193,216)', 'RGBA(0,221,255)'],
  ['RGBA(49,114,236)', 'RGBA(39,154,250)'],
  ['RGBA(227,152,3)', 'RGBA(247,211,139)'],
  ['RGBA(240,39,224)', 'RGBA(234,108,224)'],
  ['RGBA(128,35,184)', 'RGBA(175,100,222)'],
  ['RGBA(3,15,227)', 'RGBA(3,15,227)'],
  ['RGBA(255,144,26)', 'RGBA(255,232,127)']
]
const colorArrayLinear3 = [
  ['RGBA(49,114,236)', 'RGBA(39,154,250)'],
  ['RGBA(80,193,185)', 'RGBA(80,193,185)'],
  ['RGBA(15,70,232)', 'RGBA(46,93,227)'],
  ['RGBA(222,128,28)', 'RGBA(232,162,85)'],
  ['RGBA(128,35,184)', 'RGBA(175,100,222)'],
  ['RGBA(3,15,227)', 'RGBA(3,15,227)'],
  ['RGBA(255,144,26)', 'RGBA(255,232,127)']
]

const mapFileName = {
  '运城市': import('./region/yuncheng.json'),
  '盐湖区': import('./region/140800/140802'),
  '临猗县': import('./region/140800/140821'),
  '万荣县': import('./region/140800/140822'),
  '闻喜县': import('./region/140800/140823'),
  '稷山县': import('./region/140800/140824'),
  '新绛县': import('./region/140800/140825'),
  '绛县': import('./region/140800/140826'),
  '垣曲县': import('./region/140800/140827'),
  '夏县': import('./region/140800/140828'),
  '平陆县': import('./region/140800/140829'),
  '芮城县': import('./region/140800/140830'),
  '永济市': import('./region/140800/140881'),
  '河津市': import('./region/140800/140882')
}

const areaList = [
  { areaName: '盐湖区', areaCode: '140802' },
  { areaName: '临猗县', areaCode: '140821' },
  { areaName: '万荣县', areaCode: '140822' },
  { areaName: '闻喜县', areaCode: '140823' },
  { areaName: '稷山县', areaCode: '140824' },
  { areaName: '新绛县', areaCode: '140825' },
  { areaName: '绛县', areaCode: '140826' },
  { areaName: '垣曲县', areaCode: '140827' },
  { areaName: '夏县', areaCode: '140828' },
  { areaName: '平陆县', areaCode: '140829' },
  { areaName: '芮城县', areaCode: '140830' },
  { areaName: '永济市', areaCode: '140881' },
  { areaName: '河津市', areaCode: '140882' }
]

// areaList转map,key是areaName value是对象
const areaListMap = {
  '盐湖区': { areaName: '盐湖区', areaCode: '140802' },
  '临猗县': { areaName: '临猗县', areaCode: '140821' },
  '万荣县': { areaName: '万荣县', areaCode: '140822' },
  '闻喜县': { areaName: '闻喜县', areaCode: '140823' },
  '稷山县': { areaName: '稷山县', areaCode: '140824' },
  '新绛县': { areaName: '新绛县', areaCode: '140825' },
  '绛县': { areaName: '绛县', areaCode: '140826' },
  '垣曲县': { areaName: '垣曲县', areaCode: '140827' },
  '夏县': { areaName: '夏县', areaCode: '140828' },
  '平陆县': { areaName: '平陆县', areaCode: '140829' },
  '芮城县': { areaName: '芮城县', areaCode: '140830' },
  '永济市': { areaName: '永济市', areaCode: '140881' },
  '河津市': { areaName: '河津市', areaCode: '140882' }
}

const defaultWidth = 1920
const defaultHeight = 1080
const defaultWHRate = 1.777
const defaultMinFontSize = 12

// 根据页面大小调整字体大小
const resizePage = function() {
  (function(doc, win) {
    const docEl = doc.documentElement
    const resizeEvt = 'orientationchange' in window ? 'orientationchange' : 'resize'
    const clientWidth = docEl.clientWidth
    // const clientHeight = docEl.clientHeight
    const recalc = function() {
      if (!clientWidth) return
      if (clientWidth >= defaultWidth) {
        docEl.style.fontSize = defaultMinFontSize + itemUnit
      } else {
        // docEl.style.fontSize = ((defaultMinFontSize * 1.875) / (defaultWidth / clientWidth)) + itemUnit
        docEl.style.fontSize = (defaultMinFontSize / (defaultWidth / clientWidth)) + itemUnit
      }
      // const height = parseInt(clientWidth / defaultWHRate)
      // const top = parseInt((clientHeight - height) / 2)
      // if (clientHeight > height) {
      //   pageMarginTop = top + itemUnit
      //   pageHeight = height + itemUnit
      // } else {
      //   pageMarginTop = 0 + itemUnit
      //   pageHeight = clientHeight + itemUnit
      // }
      // console.log(docEl.style.fontSize)
      // docEl.style.fontSize = parseFloat(clientWidth / 1900) + 'rem'
    }
    if (!doc.addEventListener) return
    recalc()
    win.addEventListener(resizeEvt, recalc, false)
    doc.addEventListener('DOMContentLoaded', recalc, false)
  })(document, window)
}

// 根据页面大小调整页面
const resizeContainer = function() {
  (function(doc, win) {
    const docEl = doc.documentElement
    const resizeEvt = 'orientationchange' in window ? 'orientationchange' : 'resize'
    const recalc = function() {
      const clientWidth = docEl.clientWidth
      const clientHeight = docEl.clientHeight
      const visuContainer = document.getElementById('visu-container')
      // 整体内容的位置
      if (visuContainer) {
        const height = parseInt(clientWidth / defaultWHRate)
        const top = parseInt((clientHeight - height) / 2)
        if (clientHeight > height) {
          visuContainer.style.marginTop = top + itemUnit
          visuContainer.style.height = height + itemUnit
        } else {
          visuContainer.style.height = clientHeight + itemUnit
        }
        // 系统切换模块的位置
        const visuParent = document.getElementById('visu-parent')
        const visuSysModule = document.getElementById('visu-sys-module')
        // console.log('visuParent:' + visuParent)
        // console.log('top:' + top)
        if (visuParent && visuSysModule) {
          // visuSysModule.style.top = (parseInt(top / clientHeight * 100 - 7) + parseInt(visuParent.style.height.replace('%', ''))) + itemPercentUnit
          visuSysModule.style.top = parseInt(visuParent.style.height.replace('%', '')) + itemPercentUnit
        }
      }
    }
    if (!doc.addEventListener) return
    recalc()
    win.addEventListener(resizeEvt, recalc, false)
    doc.addEventListener('DOMContentLoaded', recalc, false)
  })(document, window)
}

// 删除定时
const clearInterval = function(obj) {
  if (obj != null) {
    window.clearInterval(obj)
    obj = null
  }
  return null
}

// echart定时切换tip
const chartSeriesTipInterval = function(chart, option, timer) {
  clearInterval(timer)
  chart.currentIndex = -1
  timer = setInterval(function() {
    const dataLen = option.series[0].data.length
    chart.dispatchAction({
      type: 'downplay',
      seriesIndex: 0,
      dataIndex: chart.currentIndex
    })
    chart.currentIndex = (chart.currentIndex + 1) % dataLen
    // 高亮当前图形
    chart.dispatchAction({
      type: 'showTip',
      seriesIndex: 0,
      dataIndex: chart.currentIndex
    })
  }, tipTimer)
  return timer
}

// echart定时切换tip
const chartxAxisTipInterval = function(chart, option, timer) {
  clearInterval(timer)
  chart.currentIndex = -1
  timer = setInterval(function() {
    const dataLen = option.xAxis[0].data.length
    chart.currentIndex = (chart.currentIndex + 1) % dataLen
    // 高亮当前图形
    chart.dispatchAction({
      type: 'highlight',
      seriesIndex: 0,
      dataIndex: chart.currentIndex
    })
    chart.dispatchAction({
      type: 'showTip',
      seriesIndex: 0,
      dataIndex: chart.currentIndex
    })
  }, tipTimer)
  return timer
}

// echart定时切换tip
const chartyAxisTipInterval = function(chart, option, timer) {
  clearInterval(timer)
  chart.currentIndex = -1
  timer = setInterval(function() {
    const dataLen = option.yAxis.data.length
    chart.currentIndex = (chart.currentIndex + 1) % dataLen
    // 高亮当前图形
    chart.dispatchAction({
      type: 'highlight',
      seriesIndex: 0,
      dataIndex: chart.currentIndex
    })
    chart.dispatchAction({
      type: 'showTip',
      seriesIndex: 0,
      dataIndex: chart.currentIndex
    })
  }, tipTimer)
  return timer
}

// echart定时切换颜色
const chartMapInterval = function(chart, mapOption, timer, colorType) {
  clearInterval(timer)
  // 定时变色
  timer = setInterval(function() {
    const thisOption = mapOption
    if (colorType === '0') {
      colorType = '1'
      thisOption.geo.itemStyle.normal.areaColor.colorStops = [{
        offset: 0, color: 'rgba(0,150,255, 0.4)' // 0% 处的颜色
      }, {
        offset: 0.1, color: 'rgba(0,150,255, 0.2)' // 0% 处的颜色
      }, {
        offset: 1, color: 'rgba(0,150,255, 0)' // 100% 处的颜色
      }]
      // option.geo.itemStyle.normal.borderColor = 'rgba(43, 196, 243, 1)'
    } else if (colorType === '1') {
      colorType = '0'
      thisOption.geo.itemStyle.normal.areaColor.colorStops = [{
        offset: 0, color: 'rgba(0,150,255, 0)' // 0% 处的颜色
      }, {
        offset: 0.1, color: 'rgba(0,150,255, 0.1)' // 0% 处的颜色
      }, {
        offset: 1, color: 'rgba(0,150,255, 0.2)' // 100% 处的颜色
      }]
      // option.geo.itemStyle.normal.borderColor = 'rgba(3,150,197, 1)'
    }
    chart.clear()
    chart.setOption(thisOption)
  }, tipTimer)
  return timer
}

// echart定时切换颜色
const chartMapAreaInterval = function(chart, mapOption, timer) {
  clearInterval(timer)
  // 定时变色
  let currentIndex = -1
  const dataLen = mapOption.series[0].data.length
  timer = setInterval(function() {
    // 默认高亮
    chart.dispatchAction({
      type: 'downplay',
      seriesIndex: 0
    })
    chart.dispatchAction({
      type: 'highlight',
      seriesIndex: 0,
      dataIndex: currentIndex
    })
    chart.dispatchAction({
      type: 'showTip',
      seriesIndex: 0,
      dataIndex: currentIndex
    })
    currentIndex = (currentIndex + 1) % dataLen
  }, tipTimer)
  return timer
}

// copyJSON
const copyJSON = function(option) {
  return JSON.parse(JSON.stringify(option))
}

// 跳转route
const gotoPage = function(obj, name, params) {
  obj.$router.push({ name: name, params: params })
}

const getMaxData = function(data) {
  if (Array.isArray(data)) {
    return Math.round(Math.max(...data) * 1.2)
  } else {
    return Math.round(data * 1.2)
  }
}

const cutDesc = function(object, start, num) {
  if (num < object.length) {
    return object.substring(start, num)
  }
  return object
}

// 计算最大值
const calMax = function(arr) {
  return Math.max.apply(this, arr)
  // const maxint = Math.ceil(max * 9.5 / 90)
  // // 不让最高的值超过最上面的刻度
  // const maxval = maxint * 10
  // // 让显示的刻度是整数
  // return maxval
}

// 计算最小值
const calMin = function(arr) {
  const min = Math.min.apply(this, arr)
  let minint
  if (min >= 0) minint = Math.floor(min / 12)
  else minint = Math.floor(min / 9)
  const minval = minint * 10
  // 让显示的刻度是整数
  return minval
}

// 判断传入是否为null
const isNull = function(item) {
  if (typeof (item) === 'undefined' || item == null || item === '' || item === 'undefined' || item === 'null') {
    return true
  }
  return false
}

/**
 * stepLength：一次滚动步长
 * speed：滚动速度
 * delay：停留时间
 * element：Element对象
 *
 * element.offsetHeight 元素的像素高度（包括边框和填充）
 * element.scrollTop 元素的内容垂直滚动的像素
 * element.scrollHeight 元素的高度（包括带滚动条的隐蔽的地方）
 */
let timerOrgHonor = null
let timerMemberHonor = null
const autoScrollVertical = function(stepLength, speed, delay, element, maxHeightStep) {
  timerMemberHonor = clearInterval(timerOrgHonor)
  const maxHeight = stepLength * maxHeightStep
  let step = 1
  element.scrollTop = 0

  function start() {
    timerOrgHonor = setInterval(scrolling, speed)
    element.scrollTop += step
  }

  function scrolling() {
    if (element.scrollTop >= maxHeight) {
      element.scrollTop = 1
      timerMemberHonor = (timerOrgHonor)
      setTimeout(start, delay)
    } else {
      if (element.scrollTop % stepLength !== 0) {
        element.scrollTop += step
      } else {
        if (element.scrollTop === 0 || element.scrollTop === (element.scrollHeight - element.offsetHeight)) { // 触顶或触底
          step *= -1 // 转换方向
        }
        timerMemberHonor = clearInterval(timerOrgHonor)
        setTimeout(start, delay)
      }
    }
  }

  // if (element.offsetHeight !== element.scrollHeight) { // 元素内容没有溢出时，不触发
  setTimeout(start, delay)
  // }
}
const autoScrollHorizontal = function(stepLength, speed, delay, element, maxWidthStep) {
  timerMemberHonor = clearInterval(timerMemberHonor)
  const maxWidth = stepLength * maxWidthStep
  let step = 1
  element.scrollLeft = 0

  function start() {
    timerMemberHonor = setInterval(scrolling, speed)
    element.scrollLeft += step
  }

  function scrolling() {
    if (element.scrollLeft >= maxWidth) {
      element.scrollLeft = 1
      timerMemberHonor = clearInterval(timerMemberHonor)
      setTimeout(start, delay)
    } else {
      if (element.scrollLeft % stepLength !== 0) {
        element.scrollLeft += step
      } else {
        if (element.scrollLeft === 0 || element.scrollLeft === (element.scrollWidth - element.offsetWidth)) { // 触顶或触底
          step *= -1 // 转换方向
        }
        timerMemberHonor = clearInterval(timerMemberHonor)
        setTimeout(start, delay)
      }
    }
  }

  // if (element.offsetHeight !== element.scrollHeight) { // 元素内容没有溢出时，不触发
  setTimeout(start, delay)
  // }
}

export default {
  numSeparator,
  numSeparatorDou,
  defaultAreaCode,
  defaultAreaCodeData,
  defaultProvinceList,
  defaultAreaEnd,
  defaultAreaName,
  defaultSmallAreaName,
  defaultYhAreaCode,
  defaultYhAreaCodeData,
  defaultYhAreaName,
  defaultYhSmallAreaName,
  resizePage,
  resizeContainer,
  clearInterval,
  dataDuration,
  startVal,
  minClientWidth,
  tipBgColor,
  tipTimer,
  entImg,
  chartSeriesTipInterval,
  chartxAxisTipInterval,
  chartyAxisTipInterval,
  chartMapInterval,
  chartMapAreaInterval,
  copyJSON,
  itemUnit,
  colorArray,
  colorArrayLinear,
  colorArrayLinear2,
  colorArrayLinear3,
  defaultHeight,
  defaultWidth,
  defaultWHRate,
  defaultMinFontSize,
  getMaxData,
  colorWhite,
  colorBlack,
  colorGrey,
  gotoPage,
  cutDesc,
  calMax,
  calMin,
  isNull,
  defaultNum,
  chartPushTimer,
  chartDefaultAxisNum,
  autoScrollVertical,
  autoScrollHorizontal,
  timerOrgHonor,
  timerMemberHonor,
  mapFileName,
  areaList,
  areaListMap
}
