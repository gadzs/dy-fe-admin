export function elValidatePhone(rule, value, callback) {
  if (value && !/^1[3456789]\d{9}$/.test(value) && !/^((0\d{2,3})-)(\d{7,8})(-(\d[1-4]))?$/.test(value)) {
    callback(new Error('请输入正确的手机号'))
  } else {
    callback()
  }
}

export function elValidateEmail(rule, value, callback) {
  if (value && !/^[a-z0-9]+([._\-]*[a-z0-9])*@([a-z0-9]+[-a-z0-9]*[a-z0-9]+.){1,63}[a-z0-9]+$/.test(value)) {
    callback(new Error('请输入正确的邮箱'))
  } else {
    callback()
  }
}

export function elValidatePassword(rule, value, callback) {
  if (!(/^[A-Za-z0-9\d=!\-@._*&^%$#@]*$/.test(value) && // consists of only these
    /[a-zA-Z]/.test(value) && // has a letter
    /\d/.test(value))) {
    callback(new Error('密码不能少于 8 位字符, 必须包含字母和数字'))
  } else {
    callback()
  }
}

export function elValidateVerifyCode(rule, value, callback) {
  if (!/^\d{6}$/.test(value)) {
    callback(new Error('请输入正确的验证码'))
  } else {
    callback()
  }
}

export function elValidateInteger(rule, value, callback) {
  if (!/^-?\d+$/.test(value)) {
    callback(new Error('请输入整数'))
  } else {
    callback()
  }
}

export function elValidateIdCard(rule, value, callback) {
  if (value && !/^[1-9]\d{5}(18|19|20|(3\d))\d{2}((0[1-9])|(1[0-2]))(([0-2][1-9])|10|20|30|31)\d{3}[0-9X|x]$/.test(value)) {
    callback(new Error('请输入正确的身份证号'))
  } else {
    callback()
  }
}

export function elValidateUscc(rule, value, callback) {
  if (value && !/^([0-9A-HJ-NPQRTUWXY]{2}\d{6}[0-9A-HJ-NPQRTUWXY]{10}|[1-9]\d{14})$/.test(value)) {
    callback(new Error('请输入正确的统一社会信用代码'))
  } else {
    callback()
  }
}

export const elRequiredRule = { required: true, message: '必填', trigger: 'blur' }
export const elRequiredChangeRule = { required: true, message: '必填', trigger: 'change' }
export const elPhoneNumRule = { trigger: 'blur', validator: elValidatePhone }
export const elPasswordRule = { trigger: 'blur', validator: elValidatePassword }
export const elIdCardRule = { trigger: 'blur', validator: elValidateIdCard }
export const elUsccRule = { trigger: 'blur', validator: elValidateUscc }
export const elEmailRule = { trigger: 'blur', validator: elValidateEmail }
