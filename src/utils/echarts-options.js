import visuUtil from '@/utils/visu-util'
import * as echarts from 'echarts'

const defaultColor = '#ffffff'
const animationDuration = 2000
const animationEasing = 'quarticInOut'
const animationThreshold = 20
const defaultChangeColor = '#C6CBE3'

// 白色背景黑色字体
const axisNameColor = 'rgba(0, 0, 0, 0.6)'
const axisLabelColor = 'rgba(0, 0, 0, 0.5)'
const axisSplitColor = 'rgba(0, 0, 0, 0.1)'
// 深色背景白色字体
const axisNameWhiteColor = 'rgba(255, 255, 255, 0.6)'
const axisLabelWhiteColor = 'rgba(255, 255, 255, 0.5)'
const axisSplitWhiteColor = 'rgba(255, 255, 255, 0.1)'

const borderColor = 'rgba(21, 113, 255, 1)' // rgba(21, 113, 255, 0.4)
const backgroundColor = 'rgba(0, 0, 0, 0.4)' // rgba(21, 113, 255, 0.5)

// 图表默认字体
const reLoadDocument = function() {
  const documentFontSize = (document.documentElement.style.fontSize).replace('px', '')
  const defaultFontSize = parseInt(documentFontSize * 1.3)
  const midFontSize = parseInt(documentFontSize * 1.6)
  const mapFontSize = parseInt(documentFontSize * 1.5)
  mapOption.geo.label.normal.fontSize = mapFontSize
  mapOption.geo.label.emphasis.fontSize = mapFontSize
  pieAreaOption.legend.textStyle.color = axisLabelWhiteColor
  pieAreaOption.legend.textStyle.fontSize = defaultFontSize
  pieAreaOption.series[0].label.emphasis.textStyle.fontSize = midFontSize
  polarBarOption.legend.textStyle.fontSize = defaultFontSize
  polarBarOption.radiusAxis.nameTextStyle.fontSize = defaultFontSize
  horizontalBarOption.legend.textStyle.fontSize = defaultFontSize
  horizontalBarOption.tooltip.textStyle.fontSize = midFontSize
  horizontalBarOption.xAxis.axisLabel.textStyle.fontSize = defaultFontSize
  horizontalBarOption.yAxis.axisLabel.textStyle.fontSize = defaultFontSize
  barOption.legend.textStyle.fontSize = defaultFontSize
  barOption.tooltip.textStyle.fontSize = defaultFontSize
  barOption.xAxis[0].axisLabel.textStyle.fontSize = defaultFontSize
  barOption.yAxis[0].axisLabel.textStyle.fontSize = defaultFontSize
  barOption.yAxis[0].nameTextStyle.fontSize = defaultFontSize
  barOption.legend.textStyle.color = axisLabelWhiteColor
  gaugeOption.series[0].title.fontSize = defaultFontSize
  gaugeOption.series[0].detail.fontSize = defaultFontSize
  gaugeRateOption.series[0].axisLabel.fontSize = parseInt(documentFontSize * 2)
  gaugeRateOption.series[0].detail.fontSize = parseInt(documentFontSize * 2)
  commonOption.title.textStyle.fontSize = mapFontSize
  return defaultFontSize
}

// 页面字体
const getDocumentFontSize = function() {
  const documentFontSize = (document.documentElement.style.fontSize).replace('px', '')
  return documentFontSize
}

const getDefaultFontSize = function() {
  const documentFontSize = (document.documentElement.style.fontSize).replace('px', '')
  const defaultFontSize = parseInt(documentFontSize * 1.33)
  return defaultFontSize
}

const defaultData = (function() {
  return []
})()
const commonYAxis = function(fontSize) {
  return {
    type: 'value',
    min: 0,
    splitLine: {
      show: false
    },
    splitNumber: 3,
    color: axisLabelWhiteColor,
    axisLabel: {
      formatter: '{value}',
      textStyle: {
        fontSize: fontSize,
        color: axisLabelWhiteColor
      }
    },
    nameTextStyle: {
      fontSize: fontSize
    }
  }
}
const commonOption = {
  minData: 0,
  splitNumber: 3,
  barWidth: 50,
  barThinWidth: 30,
  fontSize: 24,
  startIndex: 0,
  title: {
    text: '',
    left: 'center',
    textStyle: {
      color: axisLabelColor,
      fontWeight: 'normal'
    }
  }
}

// echart工具
const echartUtils = {
  getTitle: function(titleText) {
    const title = visuUtil.copyJSON(commonOption.title)
    title.text = titleText
    return title
  },
  getBarSeries: function(name, color, barBorderRadius, label, yAxisIndex, data) {
    const series = {
      name: name,
      type: 'bar',
      barGap: '75%',
      itemStyle: {
        normal: {
          color: color
        }
      },
      lineStyle: {
        width: 1
      },
      yAxisIndex: yAxisIndex,
      data: data
    }
    if (label != null) {
      series.label = label
    }
    if (barBorderRadius != null) {
      series.itemStyle.normal.barBorderRadius = barBorderRadius
    }
    return visuUtil.copyJSON(series)
  },
  getLineSeries: function(name, smooth, color, label, areaStyle, yAxisIndex, data) {
    const series = {
      name: name,
      type: 'line',
      smooth: smooth,
      itemStyle: {
        normal: {
          color: color
        }
      },
      lineStyle: {
        width: 1
      },
      yAxisIndex: yAxisIndex,
      data: data
    }
    if (label != null) {
      series.label = label
    }
    if (areaStyle != null) {
      series.areaStyle = areaStyle
    }
    return visuUtil.copyJSON(series)
  },

  // 更新数据
  updateData: function(option, arrayX, arrayY, index) {
    if (arrayX != null) {
      option.xAxis[index].data.shift()
      option.xAxis[index].data = arrayX
    }
    if (arrayY != null) {
      option.series[index].data.shift()
      option.series[index].data = arrayY
    }
  },

  // 更新数据
  updateDataReverse: function(option, arrayX, arrayY, index) {
    if (arrayY != null) {
      option.yAxis[index].data.shift()
      option.yAxis[index].data = arrayY
    }
    if (arrayX != null) {
      option.series[index].data.shift()
      option.series[index].data = arrayX
    }
  },

  // 平衡左右Y轴
  balanceDoubleY: function(option, array, index, splitNumber, minZero, intFlag) {
    let minData = visuUtil.calMin(array)
    const maxData = Math.round(visuUtil.calMax(array) * 1.5)
    if (minZero != null) {
      minData = 0
    }
    option.yAxis[index].min = minData
    option.yAxis[index].max = maxData
    option.yAxis[index].splitNumber = splitNumber
    if (intFlag) {
      option.yAxis[index].interval = Math.round((maxData - minData) / splitNumber)
    } else {
      option.yAxis[index].interval = (maxData - minData) / splitNumber
    }
  },
  // 平衡上下X轴
  balanceDoubleX: function(option, array, index, splitNumber, minZero, intFlag) {
    let minData = visuUtil.calMin(array)
    const maxData = visuUtil.calMax(array)
    if (minZero != null) {
      minData = 0
    }
    option.xAxis[index].min = minData
    option.xAxis[index].max = maxData
    option.xAxis[index].splitNumber = splitNumber
    if (intFlag) {
      option.xAxis[index].interval = Math.round((maxData - minData) / splitNumber)
    } else {
      option.xAxis[index].interval = (maxData - minData) / splitNumber
    }
  },

  // 平衡Y轴
  balanceOneY: function(option, array, splitNumber, minZero, intFlag) {
    let minData = visuUtil.calMin(array)
    const maxData = Math.round(visuUtil.calMax(array) * 1.5)
    if (minZero != null) {
      minData = 0
    }
    option.yAxis[0].min = minData
    option.yAxis[0].max = maxData
    option.yAxis[0].splitNumber = splitNumber
    if (intFlag) {
      option.yAxis[0].interval = Math.round((maxData - minData) / splitNumber)
    } else {
      option.yAxis[0].interval = (maxData - minData) / splitNumber
    }
  },
  // 平衡Y轴
  balanceOneYByMinMax: function(option, minData, maxData, splitNumber, intFlag) {
    option.yAxis[0].min = minData
    option.yAxis[0].max = maxData
    option.yAxis[0].splitNumber = splitNumber
    if (intFlag) {
      option.yAxis[0].interval = Math.round((maxData - minData) / splitNumber)
    } else {
      option.yAxis[0].interval = (maxData - minData) / splitNumber
    }
  },
  // 平衡X轴
  balanceOneX: function(option, array, splitNumber, minZero, intFlag) {
    let minData = visuUtil.calMin(array)
    const maxData = visuUtil.calMax(array)
    if (minZero != null) {
      minData = 0
    }
    option.xAxis[0].min = minData
    option.xAxis[0].max = maxData
    option.xAxis[0].splitNumber = splitNumber
    if (intFlag) {
      option.xAxis[0].interval = Math.round((maxData - minData) / splitNumber)
    } else {
      option.xAxis[0].interval = (maxData - minData) / splitNumber
    }
  }
}

const map3dOption = {
  geo3D: { // 地理坐标系组件用于地图的绘制
    map: '运城', // 表示中国地图
    roam: false, // 是否开启鼠标缩放和平移漫游
    aspectScale: 1.1, // 长宽比
    zoom: 1.50, // 当前视角的缩放比例（地图的放大比例）
    // top:20,
    label: {
      normal: {
        show: true,
        // fontSize: mapFontSize,
        color: 'rgba(72,240,242, 1)' // 文字颜色
      },
      emphasis: {
        show: true,
        color: '#f2f2f2'
      }
    },
    shading: 'lambert',
    light: { // 光照阴影
      main: {
        color: 'rgba(0,42,119, 1)', // 光照颜色
        intensity: 1.2, // 光照强度
        //  shadowQuality: 'high', //阴影亮度
        shadow: true, // 是否显示阴影
        alpha: 55,
        beta: 10
      },
      ambient: {
        intensity: 0.3
      }
    },
    regions: []
  },
  series: []
}

// 地图
const mapTooltip = {
  // trigger: 'item',
  enterable: true,
  // triggerOn: 'click',
  // position: 'left',
  textStyle: {
    color: defaultColor,
    backgroundColor: ''
  },
  borderRadius: 8,
  borderWidth: 2,
  borderColor: borderColor,
  backgroundColor: backgroundColor
}
const mapOption = {
  animation: true, // 默认是true  是否开启动画
  animationDuration: animationDuration, // 动画时长 ms
  animationEasing: animationEasing, // 缓动动画
  animationThreshold: animationThreshold, // 动画阈值 单种动画超过10 就会关闭动画
  geo: { // 地理坐标系组件用于地图的绘制
    map: '盐湖', // 表示中国地图
    roam: true, // 是否开启鼠标缩放和平移漫游
    aspectScale: 1.9, // 长宽比
    zoom: 1.2, // 当前视角的缩放比例（地图的放大比例）
    // top:20,
    label: {
      normal: {
        show: true,
        // fontSize: mapFontSize,
        color: 'rgba(72,240,242, 1)' // 文字颜色
      },
      emphasis: {
        show: true,
        color: '#f2f2f2'
      }
    },
    itemStyle: { // 地图区域的多边形 图形样式。
      normal: {
        areaColor: {
          type: 'linear',
          x: 0,
          y: 0,
          x2: 0,
          y2: 1,
          colorStops: [{
            offset: 0, color: 'rgba(0,150,255, 0)' // 0% 处的颜色
          }, {
            offset: 0.1, color: 'rgba(0,150,255, 0.1)' // 0% 处的颜色
          }, {
            offset: 1, color: 'rgba(0,150,255, 0.2)' // 100% 处的颜色
          }],
          globalCoord: false // 缺省为 false
        },
        // areaColor: 'rgba(43, 196, 243, 0.42)',
        borderColor: 'rgba(0,150,255, 1)',
        borderWidth: 3,
        shadowColor: 'rgba(0,150,255, 0.6)',
        // shadowColor: 'rgba(29,37,132,1)',
        shadowBlur: 5,
        shadowOffsetX: 5,
        shadowOffsetY: 7
      },
      emphasis: {
        areaColor: 'rgba(0,150,255, .6)'
      }
    },
    regions: []
  },
  series: []
}

// 饼图
const pieLineLabelItemStyle = function(fontSize) {
  return {
    normal: {
      borderRadius: 8,
      borderColor: 'rgba(2,25,74,1)',
      borderWidth: 2,
      label: {
        show: true,
        position: 'outside',
        color: defaultColor,
        // color: 'inherit',
        // fontSize: fontSize,
        formatter: '{d}%'
      },
      // itemStyle: {
      //   // textStyle: {
      //   //   // fontSize: parseInt(fontSize * 0.1)
      //   //   // fontSize: 1
      //   // }
      // },
      labelLine: {
        length: 12,
        length2: 20,
        show: true
      }
    }
  }
}
const pieAreaOption = {
  legend: {
    orient: 'vertical', // horizontal
    // right: '1%',
    // top: 'middle',
    // bottom: '1%',
    x: 'right',
    y: 'middle',
    itemHeight: 10,
    itemWidth: 10,
    textStyle: {
      // fontSize: defaultFontSize,
      backgroundColor: '',
      color: defaultColor
    }
  },
  tooltip: {
    trigger: 'item',
    formatter: '{b}: {c} ({d}%)',
    textStyle: {
      color: defaultColor,
      backgroundColor: ''
    },
    borderColor: borderColor,
    backgroundColor: backgroundColor,
    enterable: true
  },
  series: [
    {
      name: '面积模式',
      type: 'pie',
      center: ['50%', '50%'],
      // roseType: 'area',
      itemStyle: {
        // shadowColor: 'grey',
        // shadowBlur: 10,
        borderWidth: 20,
        borderColor: 'rgba(2,25,74,1)',
        borderRadius: 3
      },
      label: {
        show: true,
        position: 'center',
        color: defaultColor,
        formatter: '{c} ({d}%)',
        emphasis: {
          show: false,
          textStyle: {
            // fontSize: defaultFontSize,
            fontWeight: 'bold'
          }
        }
      },
      emphasis: {
        show: false,
        textStyle: {
          // fontSize: defaultFontSize,
          fontWeight: 'bold'
        }
      },
      data: [],
      animation: true, // 默认是true  是否开启动画
      animationDuration: animationDuration, // 动画时长 ms
      animationEasing: animationEasing, // 缓动动画
      animationThreshold: animationThreshold // 动画阈值 单种动画超过10 就会关闭动画
    }
  ]
}

// polar柱状图
const polarBarOption = {
  animation: true, // 默认是true  是否开启动画
  animationDuration: animationDuration, // 动画时长 ms
  animationEasing: animationEasing, // 缓动动画
  animationThreshold: animationThreshold, // 动画阈值 单种动画超过10 就会关闭动画
  angleAxis: {
    show: false
  },
  polar: {},
  grid: {
    top: '30%',
    left: '5%',
    right: '4%',
    bottom: '0%'
  },
  textStyle: {
    color: defaultColor
  },
  itemStyle: {
    normal: {
      color: defaultColor,
      label: {
        textStyle: {
          color: defaultColor
        }
      }
    },
    borderRadius: 8,
    shadowColor: 'grey',
    shadowBlur: 20
  },
  backgroundColor: '',
  legend: {
    data: [],
    orient: 'vertical',
    x: 'right',
    y: 'center',
    itemHeight: 10,
    itemWidth: 10,
    textStyle: {
      // fontSize: defaultFontSize,
      color: defaultColor
    }
  },
  radiusAxis: {
    type: 'category',
    data: [],
    z: 10,
    nameTextStyle: {
      // fontSize: defaultFontSize,
      color: defaultColor
    }
  },
  tooltip: {
    trigger: 'item',
    formatter: '{a} : {c}',
    textStyle: {
      color: defaultColor
    },
    enterable: true
  },
  series: []
}

// 横向柱状图
const horizontalBarOption = {
  animation: true, // 默认是true  是否开启动画
  animationDuration: animationDuration, // 动画时长 ms
  animationEasing: animationEasing, // 缓动动画
  animationThreshold: animationThreshold, // 动画阈值 单种动画超过10 就会关闭动画
  itemStyle: {
    normal: {
      color: defaultColor,
      label: {
        textStyle: {
          color: defaultColor
        }
      }
    }
  },
  tooltip: {
    trigger: 'axis',
    axisPointer: {
      type: 'shadow'
    },
    textStyle: {
      color: defaultColor,
      align: 'left'
    },
    borderColor: '#25B8E3',
    backgroundColor: 'rgba(1, 97, 210, 0.8)'
  },
  legend: {
    y: 'top',
    left: 'right',
    data: [],
    itemHeight: 10,
    itemWidth: 10,
    textStyle: {
      // fontSize: defaultFontSize,
      color: axisLabelColor
    }
  },
  grid: {
    left: '5%',
    right: '10%',
    bottom: '2%',
    top: '10%',
    containLabel: true
  },
  xAxis: {
    type: 'value',
    axisLabel: {
      interval: 0,
      rotate: 0,
      textStyle: {
        // fontSize: defaultFontSize,
        color: axisLabelColor
      }
    },
    axisLine: {
      show: true,
      lineStyle: {
        color: axisLabelColor
      }
    },
    splitArea: {
      show: false
    },
    min: 0,
    splitLine: {
      show: false
    }
  },
  yAxis: {
    type: 'category',
    data: [],
    axisLine: {
      lineStyle: {
        color: axisLabelColor
      }
    },
    axisLabel: {
      textStyle: {
        // fontSize: defaultFontSize,
        color: axisLabelColor
      }
    }
  },
  series: []
}

// 柱状图
const commonBarOption = {
  show: true,
  valueAnimation: true,
  textStyle: {
    color: defaultColor
  }
}

function updateCommonOption(barWidth) {
  return updateBarOption(barWidth, null, true, {
    left: '5%',
    right: '4%',
    bottom: '2%',
    top: '18%',
    containLabel: true
  }, false)
}

function updateCommonOption2(barWidth) {
  return updateBarOption(barWidth, null, true, {
    left: '5%',
    right: '4%',
    bottom: '10%',
    top: '12%',
    containLabel: true
  }, false)
}

const defaultGrid = {
  left: '5%',
  right: '4%',
  bottom: '5%',
  top: '20%',
  containLabel: true
}

function updateBarOption(barWidth, rotate, showYSplitLine, grid, splitArea) {
  const option = JSON.parse(JSON.stringify(barOption))
  if (barWidth != null) {
    option.barWidth = barWidth
  }
  if (rotate != null) {
    option.xAxis[0].axisLabel.rotate = rotate
  }
  option.yAxis[0].splitLine.show = showYSplitLine
  option.yAxis[0].splitArea.show = splitArea
  if (grid != null) {
    option.grid = grid
  }
  return option
}

function updateWhiteBarOption(barWidth, rotate, showYSplitLine, grid, splitArea) {
  const option = updateBarOption(barWidth, rotate, showYSplitLine, grid, splitArea)
  return updateBarWhiteColor(option)
}

function getLineSeries(name, color, data) {
  return {
    name: name,
    type: 'line',
    smooth: true, // 平滑曲线
    label: {
      show: true,
      position: 'top',
      color: visuUtil.colorBlack,
      fontSize: visuUtil.defaultMinFontSize
    },
    itemStyle: {
      normal: {
        color: color
      }
    },
    data: data
  }
}

function getBarSeries(name, colorArray, data) {
  return {
    name: name,
    type: 'bar',
    barGap: '75%',
    itemStyle: {
      normal: {
        color: new echarts.graphic.LinearGradient(
          0, 0, 0, 1,
          [
            { offset: 1, color: colorArray[0] },
            { offset: 0, color: colorArray[1] }
          ]
        ),
        // 柱形图圆角，初始化效果
        barBorderRadius: [8, 8, 0, 0]
      }
    },
    data: data
  }
}

function updateBarWhiteColor(option) {
  option.xAxis[0].axisLine.lineStyle.color = axisLabelWhiteColor
  option.xAxis[0].axisLabel.textStyle.color = axisLabelWhiteColor
  option.yAxis[0].nameTextStyle.color = axisNameWhiteColor
  option.yAxis[0].axisLine.lineStyle.color = axisLabelWhiteColor
  option.yAxis[0].splitLine.lineStyle.color = axisSplitWhiteColor
  option.yAxis[0].axisLabel.textStyle.color = axisLabelWhiteColor
  option.legend.textStyle.color = axisLabelWhiteColor
  return option
}

const barOption = {
  animation: true, // 默认是true  是否开启动画
  animationDuration: animationDuration, // 动画时长 ms
  animationEasing: animationEasing, // 缓动动画
  animationThreshold: animationThreshold, // 动画阈值 单种动画超过10 就会关闭动画
  tooltip: {
    trigger: 'axis',
    axisPointer: {
      type: 'shadow'
    },
    textStyle: {
      color: defaultColor,
      align: 'left'
    },
    borderColor: borderColor,
    backgroundColor: backgroundColor,
    enterable: true
  },
  backgroundColor: '',
  legend: {
    top: '2%',
    right: '3%',
    itemHeight: 10,
    itemWidth: 10,
    y: 'top',
    x: 'right',
    orient: 'horizontal',
    textStyle: {
      // fontSize: defaultFontSize,
      color: axisLabelColor
    }
  },
  xAxis: [{
    type: 'category',
    axisLine: {
      show: true,
      lineStyle: {
        color: axisLabelWhiteColor
      }
    },
    axisLabel: {
      interval: 0,
      rotate: 0,
      textStyle: {
        // fontSize: defaultFontSize,
        color: axisLabelWhiteColor
      }
    },
    axisTick: {
      show: false
    },
    splitLine: {
      show: false
    },
    data: []
  }],
  yAxis: [{
    type: 'value',
    name: '',
    nameTextStyle: {
      // fontSize: defaultFontSize,
      color: axisLabelWhiteColor
    },
    splitArea: {
      show: false
    },
    min: 0,
    splitLine: {
      show: true,
      lineStyle: {
        type: 'dashed',
        color: axisSplitColor
      }
    },
    axisLine: {
      show: true,
      lineStyle: {
        color: axisLabelWhiteColor
      }
    },
    axisTick: {
      show: false
    },
    axisLabel: {
      formatter: '{value}',
      textStyle: {
        // fontSize: defaultFontSize,
        color: axisLabelWhiteColor
      }
    },
    splitNumber: 3
  }],
  series: []
}

// 关系图
const graphOption = {
  animation: true, // 默认是true  是否开启动画
  animationDuration: animationDuration, // 动画时长 ms
  animationEasing: animationEasing, // 缓动动画
  animationThreshold: animationThreshold, // 动画阈值 单种动画超过10 就会关闭动画
  series: [
    {
      type: 'graph',
      layout: 'none',
      symbol: 'circle',
      symbolSize: 50,
      // color: 'red',
      roam: true,
      label: {
        show: true,
        position: 'bottom'
      },
      edgeSymbol: ['circle', 'arrow'],
      edgeSymbolSize: [4, 10],
      edgeLabel: {
        fontSize: 20
      },
      data: [],
      links: [],
      lineStyle: {
        opacity: 0.9,
        width: 2,
        curveness: 0.2
      }
    }
  ]
}

// 连线
const linesOption = {
  animation: true, // 默认是true  是否开启动画
  animationDuration: animationDuration, // 动画时长 ms
  animationEasing: animationEasing, // 缓动动画
  animationThreshold: animationThreshold, // 动画阈值 单种动画超过10 就会关闭动画
  backgroundColor: '',
  xAxis: [{
    show: false,
    type: 'category',
    data: []
  }],
  yAxis: [{
    show: false,
    type: 'value',
    min: 0
  }],
  series: [{ // 特效尾部(红线)
    type: 'lines',
    coordinateSystem: 'cartesian2d',
    xAxisIndex: 0,
    yAxisIndex: 0,
    zlevel: 1, // zlevel 大的 Canvas 会放在 zlevel 小的 Canvas 的上面
    effect: {
      show: true,
      period: 3,
      trailLength: 0.7,
      color: '', // arrow箭头的颜色
      symbolSize: 3,
      loop: false // 是否循环显示
    },
    lineStyle: {
      normal: {
        color: '',
        width: 0,
        curveness: 0.1 // 0-1曲度越大
      }
    },
    data: []
  }, { // 动画连接线
    type: 'lines',
    coordinateSystem: 'cartesian2d',
    zlevel: 2,
    xAxisIndex: 0,
    yAxisIndex: 0,
    // symbol: ['arrow', 'rect'], // 贴花的图案，如果是 string[] 表示循环使用数组中的图案。'circle', 'rect', 'roundRect', 'triangle', 'diamond', 'pin', 'arrow', 'none'
    // symbolSize: 10,
    effect: {
      show: true,
      period: 3, // 特效动画的时间，单位为 s。
      trailLength: 0, // 特效尾迹的长度。取从 0 到 1 的值，数值越大尾迹越长。
      symbol: 'arrow',
      loop: false, // 是否循环显示
      symbolSize: 15 // 特效标记的大小，可以设置成诸如 10 这样单一的数字，也可以用数组分开表示高和宽，例如 [20, 10] 表示标记宽为20，高为10。
    },
    lineStyle: {
      normal: {
        color: '',
        width: 1,
        opacity: 0.6,
        curveness: 0.1 // 边的曲度，支持从 0 到 1 的值，值越大曲度越大。
      }
    },
    label: {
      normal: {
        show: true,
        position: 'middle'
      }
    },
    // x轴是index值, y是数值
    data: []
  }, { // 两端顶点
    type: 'effectScatter',
    coordinateSystem: 'cartesian2d',
    zlevel: 2,
    xAxisIndex: 0,
    yAxisIndex: 0,
    rippleEffect: {
      brushType: 'stroke'
    },
    label: {
      normal: {
        show: true,
        position: 'bottom'
      }
    },
    symbolSize: 10,
    itemStyle: {
      normal: {
        color: ''
      }
    },
    data: []
  }]
}

// 雷达图
const radarOption = function(defaultFontSize) {
  return {
    animation: true, // 默认是true  是否开启动画
    animationDuration: animationDuration, // 动画时长 ms
    animationEasing: animationEasing, // 缓动动画
    animationThreshold: animationThreshold, // 动画阈值 单种动画超过10 就会关闭动画
    tooltip: {
      show: true,
      trigger: 'item'
    },
    radar: [{
      center: ['50%', '50%'], // 偏移位置
      radius: 50,
      startAngle: 70,
      splitNumber: 4,
      shape: 'circle',
      name: {
        formatter: '[{value}]',
        textStyle: {
          color: defaultColor
        }
      },
      // axisLabel: {
      //   show: false,
      //   fontSize: defaultFontSize,
      //   color: 'red',
      //   fontStyle: 'normal',
      //   fontWeight: 'normal'
      // },
      // splitArea: {
      //   areaStyle: {
      //     color: 'transparent'
      //   }
      // },
      splitArea: {
        areaStyle: {
          color: ['rgba(0,150,255, 0.2)',
            'rgba(0,150,255, 0.3)', 'rgba(0,150,255, 0.5)',
            'rgba(0,150,255, 0.6)'],
          shadowColor: 'rgba(0, 0, 0, 0.3)',
          shadowBlur: 10
        }
      },
      axisLine: {
        lineStyle: {
          color: 'rgba(255, 255, 255, 0.5)'
        }
      },
      splitLine: {
        lineStyle: {
          color: 'rgba(255, 255, 255, 0.5)'
        }
      }
    }],
    series: [{
      type: 'radar',
      itemStyle: {
        normal: {
          color: '#FF9D1A',
          lineStyle: {
            width: 1,
            color: '#FF9D1A'
          }
        },
        emphasis: {
          color: '#00DDFF',
          lineStyle: {
            width: 4,
            color: '#00DDFF'
          }
        }
      },
      data: []
    }]
  }
}

// 自定义仪表盘
const diyGaugeOption = {
  _panelImageURL: '../assets/module_images/visu/custom-gauge-panel.png',
  _animationDuration: 1000,
  _animationDurationUpdate: 1000,
  _animationEasingUpdate: 'quarticInOut',
  _valOnRadianMax: 100,
  _outerRadius: 200,
  _innerRadius: 170,
  _pointerInnerRadius: 40,
  _insidePanelRadius: 140,
  _currentDataIndex: 0,
  data: 70,

  renderItem: function(params, api) {
    const valOnRadian = api.value(1)
    const coords = api.coord([api.value(0), valOnRadian])
    const polarEndRadian = coords[3]
    const imageStyle = {
      image: diyGaugeOption._panelImageURL,
      x: params.coordSys.cx - diyGaugeOption._outerRadius,
      y: params.coordSys.cy,
      width: diyGaugeOption._outerRadius * 2,
      height: diyGaugeOption._outerRadius * 2
    }

    return {
      type: 'group',
      children: [{
        type: 'image',
        style: imageStyle,
        clipPath: {
          type: 'sector',
          shape: {
            cx: params.coordSys.cx,
            cy: params.coordSys.cy,
            r: diyGaugeOption._outerRadius,
            r0: diyGaugeOption._innerRadius,
            startAngle: 0,
            endAngle: -polarEndRadian,
            transition: 'endAngle',
            enterFrom: { endAngle: 0 }
          }
        }
      }, {
        type: 'image',
        style: imageStyle,
        clipPath: {
          type: 'polygon',
          shape: {
            points: diyGaugeOption.makePionterPoints(params, polarEndRadian)
          },
          extra: {
            polarEndRadian: polarEndRadian,
            transition: 'polarEndRadian',
            enterFrom: { polarEndRadian: 0 }
          },
          during: function(apiDuring) {
            apiDuring.setShape(
              'points',
              diyGaugeOption.makePionterPoints(params, apiDuring.getExtra('polarEndRadian'))
            )
          }
        }
      }, {
        type: 'circle',
        shape: {
          cx: params.coordSys.cx,
          cy: params.coordSys.cy,
          r: diyGaugeOption._insidePanelRadius
        },
        style: {
          fill: '#fff',
          shadowBlur: 25,
          shadowOffsetX: 0,
          shadowOffsetY: 0,
          shadowColor: 'rgba(76,107,167,0.4)'
        }
      }, {
        type: 'text',
        extra: {
          valOnRadian: valOnRadian,
          transition: 'valOnRadian',
          enterFrom: { valOnRadian: 0 }
        },
        style: {
          text: diyGaugeOption.data,
          fontSize: 50,
          fontWeight: 700,
          x: params.coordSys.cx,
          y: params.coordSys.cy,
          fill: 'rgb(0,50,190)',
          align: 'center',
          verticalAlign: 'middle',
          enterFrom: { opacity: 0 }
        },
        during: function(apiDuring) {
          apiDuring.setStyle('text', diyGaugeOption.data)
        }
      }]
    }
  },

  convertToPolarPoint: function(renderItemParams, radius, radian) {
    return [
      Math.cos(radian) * radius + renderItemParams.coordSys.cx,
      -Math.sin(radian) * radius + renderItemParams.coordSys.cy
    ]
  },

  makePionterPoints: function(renderItemParams, polarEndRadian) {
    return [
      diyGaugeOption.convertToPolarPoint(renderItemParams, diyGaugeOption._outerRadius, polarEndRadian),
      diyGaugeOption.convertToPolarPoint(renderItemParams, diyGaugeOption._outerRadius, polarEndRadian + Math.PI * 0.03),
      diyGaugeOption.convertToPolarPoint(renderItemParams, diyGaugeOption._pointerInnerRadius, polarEndRadian)
    ]
  },

  option: function() {
    return {
      animationEasing: diyGaugeOption._animationEasingUpdate,
      animationDuration: diyGaugeOption._animationDuration,
      animationDurationUpdate: diyGaugeOption._animationDurationUpdate,
      animationEasingUpdate: diyGaugeOption._animationEasingUpdate,
      dataset: {
        source: [[1, diyGaugeOption.data]]
      },
      tooltip: {},
      angleAxis: {
        type: 'value',
        startAngle: 0,
        axisLine: { show: false },
        axisTick: { show: false },
        axisLabel: { show: false },
        splitLine: { show: false },
        min: 0,
        max: diyGaugeOption._valOnRadianMax
      },
      radiusAxis: {
        type: 'value',
        axisLine: { show: false },
        axisTick: { show: false },
        axisLabel: { show: false },
        splitLine: { show: false }
      },
      polar: {},
      series: [{
        type: 'custom',
        coordinateSystem: 'polar',
        renderItem: diyGaugeOption.renderItem
      }]
    }
  }
}

// 仪表盘
const gaugeOption = {
  animation: true, // 默认是true  是否开启动画
  animationDuration: animationDuration, // 动画时长 ms
  animationEasing: animationEasing, // 缓动动画
  animationThreshold: animationThreshold, // 动画阈值 单种动画超过10 就会关闭动画
  series: [{
    type: 'gauge',
    startAngle: 90,
    endAngle: -270,
    pointer: {
      show: false
    },
    progress: {
      show: true,
      overlap: false,
      roundCap: true,
      clip: false,
      itemStyle: {
        borderWidth: 1,
        borderColor: '#464646'
      }
    },
    axisLine: {
      lineStyle: {
        width: 20
      }
    },
    splitLine: {
      show: false,
      distance: 0,
      length: 10
    },
    axisTick: {
      show: false
    },
    axisLabel: {
      show: false,
      distance: 50
    },
    data: [
      {
        title: {
          offsetCenter: ['0%', '-20%']
        },
        detail: {
          offsetCenter: ['0%', '20%']
        }
      }
    ],
    title: {
      // fontSize: defaultFontSize,
      color: '#000000'
    },
    detail: {
      width: 50,
      height: 12,
      // fontSize: defaultFontSize,
      color: '#000000',
      borderColor: 'auto',
      borderRadius: 20,
      valueAnimation: true,
      borderWidth: 0,
      formatter: '{value}%'
    }
  }]
}

// 仪表盘
const gaugeRateOption = {
  animation: true, // 默认是true  是否开启动画
  animationDuration: animationDuration, // 动画时长 ms
  animationEasing: animationEasing, // 缓动动画
  animationThreshold: animationThreshold, // 动画阈值 单种动画超过10 就会关闭动画
  series: [
    {
      type: 'gauge',
      axisLine: {
        lineStyle: {
          width: 5,
          color: [
            [0.3, '#fd666d'],
            [0.7, '#F0A33A'],
            [1, '#4E9CA4']
          ]
        }
      },
      pointer: {
        itemStyle: {
          color: 'auto'
        }
      },
      axisTick: {
        distance: -6, // 小刻度的位置，离中心越近，值越大
        length: 8,
        lineStyle: {
          color: '#fff',
          width: 1
        }
      },
      splitLine: {
        distance: -10, // 白色大间隔线，控制数字离中心点的距离，越大越近
        length: 10,
        lineStyle: {
          color: '#fff',
          width: 4
        }
      },
      axisLabel: {
        color: 'auto',
        distance: -18 // 数字离中心点的距离（数值越大，越靠近）
      },
      detail: {
        valueAnimation: true,
        formatter: '{value}%',
        color: 'auto'
      },
      data: []
    }
  ]
}
export default {
  defaultData,
  commonOption,
  commonYAxis,
  defaultColor,
  defaultChangeColor,
  axisLabelColor,
  mapOption,
  mapTooltip,
  map3dOption,
  gaugeOption,
  gaugeRateOption,
  barOption,
  polarBarOption,
  horizontalBarOption,
  updateCommonOption,
  updateCommonOption2,
  updateBarOption,
  updateWhiteBarOption,
  pieAreaOption,
  pieLineLabelItemStyle,
  commonBarOption,
  reLoadDocument,
  linesOption,
  graphOption,
  echartUtils,
  getDefaultFontSize,
  getDocumentFontSize,
  radarOption,
  defaultGrid,
  getBarSeries,
  getLineSeries,
  updateBarWhiteColor,
  diyGaugeOption,
  axisNameColor
}
