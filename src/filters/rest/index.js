import * as dict from '@/utils/dict'
import moment from 'moment'

export function yesNoStatusFilter(type) {
  return dict.yesNoStatusMap[type]
}

export function enableStatusFilter(type) {
  return dict.enableStatusMap[type]
}

export function shopStatusFilter(type) {
  return dict.shopStatusMap[type]
}

export function genderFilter(type) {
  return dict.genderMap[type]
}

export function areaFilter(id) {
  return dict.areaMap[id]
}

export function activityScopeFilter(id) {
  return dict.activityScopeMap[id]
}

export function formatDate(date, pattern = 'YYYY-MM-DD', emptyValue = '') {
  return date ? moment(date).format(pattern) : emptyValue
}

export function formatDateTime(date, pattern = 'YYYY-MM-DD HH:mm:ss', emptyValue = '') {
  return date ? moment(date).format(pattern) : emptyValue
}

export function strippedContentFilter(string) {
  const newStr = string.replace(/<\/?[^>]+>/ig, '').replace(/[ ]|[&nbsp;]|[&ldquo]/ig, '')
  if (newStr.length > 58) {
    return newStr.substring(0, 58) + '...'
  } else {
    return newStr
  }
}

export function usedStatusFilter(type) {
  return dict.usedStatusMap[type]
}

export function entityObjectTypeFilter(type) {
  return dict.entityObjectTypeMap[type]
}

export function fen2YuanFilter(fen) {
  return fen / 100 + ' 元'
}

export function activityAuditStatusFilter(type) {
  return dict.activityAuditStatusMap[type]
}
