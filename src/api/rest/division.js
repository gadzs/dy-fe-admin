import request from '@/utils/request_rest'

export function adminDivisionListByParentId(parentId) {
  return request({
    url: '/api/admin/division/listByParentId',
    method: 'post',
    params: {
      parentId
    }
  })
}
