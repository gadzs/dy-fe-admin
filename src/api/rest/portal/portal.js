import request from '@/utils/request_rest'

// 产品服务栏目
export function serviceCategory() {
  return request({
    url: '/api/portal/service/category',
    method: 'get',
    params: {}
  })
}

// 栏目列表
export function categoryList(data) {
  return request({
    url: '/api/portal/category/list',
    method: 'get',
    params: data
  })
}

// 获取栏目信息
export function getCategoryByCode(code) {
  const url = '/api/portal/category/' + code
  return request({
    url: url,
    method: 'get'
  })
}

// 文章列表
export function articleList(data) {
  return request({
    url: '/api/portal/article/list',
    method: 'get',
    params: data
  })
}

// 获取文章详情
export function getArticle(id) {
  const url = '/api/portal/article/' + id
  return request({
    url: url,
    method: 'get'
  })
}

// 查看图片
export function viewImage(id) {
  const url = '/api/portal/fileAttachmentId/' + id
  return request({
    url: url,
    method: 'get'
  })
}

// 下载附件
export function downloadAttachment(id, attachId) {
  const url = '/api/portal/downloadAttachment/' + id + '/' + attachId
  return request({
    url: url,
    method: 'get'
  })
}

// 公司认证列表
export function shopAuthLabelList() {
  return request({
    url: '/api/portal/shop/authLabel/all',
    method: 'get',
    params: {}
  })
}

// 留言板列表
export function messageboardList(data) {
  return request({
    url: '/api/portal/messageboard/list',
    method: 'get',
    params: data
  })
}

// 留言
export function messageboardCreate(data) {
  return request({
    url: '/api/portal/messageboard/create',
    method: 'post',
    data
  })
}

// 检索
export function search(data) {
  return request({
    url: '/api/portal/shop/search',
    method: 'get',
    params: data
  })
}

export function searchKnowledgeList(data) {
  return request({
    url: '/api/portal/knowledge/search',
    method: 'post',
    data: data
  })
}

