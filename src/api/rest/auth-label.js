import request from '@/utils/request_rest'

export function authLabelList(data) {
  return request({
    url: '/api/admin/shop/authLabel/list',
    method: 'get',
    params: data
  })
}

export function authLabelCreate(data) {
  return request({
    url: '/api/admin/shop/authLabel/create',
    method: 'post',
    data
  })
}

export function authLabelUpdate(data) {
  return request({
    url: '/api/admin/shop/authLabel/update',
    method: 'post',
    data
  })
}
