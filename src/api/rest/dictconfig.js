import request from '@/utils/request_rest'

export function dictConfigList(data) {
  return request({
    url: '/api/admin/dictConfig/list',
    method: 'get',
    params: data
  })
}

export function dictConfigCheckAvailable(dictKey, id) {
  return request({
    url: '/api/admin/dictConfig/checkAvailable',
    method: 'get',
    params: {
      dictKey,
      id
    }
  })
}

export function dictConfigCreate(data) {
  return request({
    url: '/api/admin/dictConfig/create',
    method: 'post',
    data
  })
}

export function dictConfigUpdate(data) {
  return request({
    url: '/api/admin/dictConfig/update',
    method: 'post',
    data
  })
}

export function dictConfigDelete(id) {
  return request({
    url: '/api/admin/dictConfig/delete',
    method: 'post',
    params: {
      id
    }
  })
}

export function dictConfigItemList(dictId, data) {
  const url = '/api/admin/dictConfig/' + dictId + '/list'
  return request({
    url: url,
    method: 'get',
    params: data
  })
}

export function dictConfigItemCheckAvailable(dictConfigId, itemValue, id) {
  return request({
    url: '/api/admin/dictConfig/' + dictConfigId + '/checkAvailable',
    method: 'get',
    params: {
      dictConfigId,
      itemValue,
      id
    }
  })
}

export function dictConfigItemCreate(dictId, data) {
  const url = '/api/admin/dictConfig/' + dictId + '/create'
  return request({
    url: url,
    method: 'post',
    data
  })
}

export function dictConfigItemUpdate(dictId, data) {
  const url = '/api/admin/dictConfig/' + dictId + '/update'
  return request({
    url: url,
    method: 'post',
    data
  })
}

export function dictConfigItemDelete(dictId, id) {
  const url = '/api/admin/dictConfig/' + dictId + '/delete'
  return request({
    url: url,
    method: 'post',
    params: {
      id
    }
  })
}

export function dictConfigItemByDictKeyList(dictKey) {
  const url = '/api/admin/dictConfig/item/list'
  return request({
    url: url,
    method: 'get',
    params: { dictConfigKey: dictKey }
  })
}
