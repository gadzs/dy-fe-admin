import request from '@/utils/request_rest'

export function adminActivitySearch(data) {
  return request({
    url: '/api/admin/activity/search',
    method: 'get',
    params: data
  })
}

export function adminActivityCreate(data) {
  return request({
    url: '/api/admin/activity/create',
    method: 'post',
    data
  })
}

export function adminActivityUpdate(data) {
  return request({
    url: '/api/admin/activity/update',
    method: 'post',
    data
  })
}

export function adminActivityDelete(id) {
  return request({
    url: `/api/admin/activity/delete/${id}`,
    method: 'get'
  })
}

export function adminActivityView(id) {
  return request({
    url: `/api/admin/activity/view/${id}`,
    method: 'get'
  })
}

export function adminActivityAuditPass(data) {
  return request({
    url: '/api/admin/activity/auditPass',
    method: 'post',
    data
  })
}

export function adminActivityAuditFailure(data) {
  return request({
    url: '/api/admin/activity/auditFailure',
    method: 'post',
    data
  })
}

export function adminActivityOpen(id) {
  return request({
    url: `/api/admin/activity/open/${id}`,
    method: 'get'
  })
}

export function adminActivityClose(id) {
  return request({
    url: `/api/admin/activity/close/${id}`,
    method: 'get'
  })
}

