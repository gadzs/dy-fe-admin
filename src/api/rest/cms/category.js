import request from '@/utils/request_rest'

export function categoryList(data) {
  return request({
    url: '/api/admin/cms/category/list',
    method: 'get',
    params: data
  })
}

export function categoryTypes() {
  const url = '/api/admin/cms/category/categoryTypes'
  return request({
    url: url,
    method: 'get'
  })
}

export function getCategory(id) {
  const url = '/api/admin/cms/category/get/' + id
  return request({
    url: url,
    method: 'get'
  })
}

export function deleteCategory(id) {
  const url = '/api/admin/cms/category/delete/' + id
  return request({
    url: url,
    method: 'post'
  })
}

export function createCategory(data) {
  return request({
    url: '/api/admin/cms/category/create',
    method: 'post',
    params: data
  })
}

export function editCategory(data) {
  return request({
    url: '/api/admin/cms/category/update',
    method: 'post',
    params: data
  })
}
