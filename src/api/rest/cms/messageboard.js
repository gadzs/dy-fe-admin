import request from '@/utils/request_rest'

export function messageboardList(data) {
  return request({
    url: '/api/admin/cms/messageboard/list',
    method: 'get',
    params: data
  })
}

export function messageboardTypes() {
  const url = '/api/admin/cms/messageboard/messageboardTypes'
  return request({
    url: url,
    method: 'get'
  })
}

export function getMessageboard(id) {
  const url = '/api/admin/cms/messageboard/get/' + id
  return request({
    url: url,
    method: 'get'
  })
}

export function deleteMessageboard(id) {
  const url = '/api/admin/cms/messageboard/delete/' + id
  return request({
    url: url,
    method: 'post'
  })
}

export function createMessageboard(data) {
  return request({
    url: '/api/admin/cms/messageboard/create',
    method: 'post',
    params: data
  })
}

export function updateMessageboard(data) {
  return request({
    url: '/api/admin/cms/messageboard/update',
    method: 'post',
    params: data
  })
}

export function replyMessageboard(data) {
  return request({
    url: '/api/admin/cms/messageboard/reply',
    method: 'post',
    params: data
  })
}

export function showMessageboard(data) {
  return request({
    url: '/api/admin/cms/messageboard/setShow',
    method: 'post',
    params: data
  })
}
