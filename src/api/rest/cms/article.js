import request from '@/utils/request_rest'

export function articleList(data) {
  return request({
    url: '/api/admin/cms/article/list',
    method: 'get',
    params: data
  })
}

export function articleTypes() {
  const url = '/api/admin/cms/article/articleTypes'
  return request({
    url: url,
    method: 'get'
  })
}

export function getArticle(id) {
  const url = '/api/admin/cms/article/get/' + id
  return request({
    url: url,
    method: 'get'
  })
}

export function articleStatus() {
  const url = '/api/admin/cms/article/articleStatus'
  return request({
    url: url,
    method: 'get'
  })
}

export function articleCreate(data) {
  return request({
    url: '/api/admin/cms/article/create',
    method: 'post',
    data
  })
}

export function articleUpdate(data) {
  return request({
    url: '/api/admin/cms/article/update',
    method: 'post',
    data
  })
}

export function articleDelete(id) {
  const url = '/api/admin/cms/article/delete/' + id
  return request({
    url: url,
    method: 'post'
  })
}
