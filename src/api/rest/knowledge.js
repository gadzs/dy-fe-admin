import request from '@/utils/request_rest'

export function adminKnowledgeSearch(data) {
  return request({
    url: '/api/admin/knowledge/search',
    method: 'get',
    params: data
  })
}

export function adminKnowledgeCreate(data) {
  return request({
    url: '/api/admin/knowledge/create',
    method: 'post',
    data
  })
}

export function adminKnowledgeUpdate(data) {
  return request({
    url: '/api/admin/knowledge/update',
    method: 'post',
    data
  })
}

export function adminKnowledgeDelete(id) {
  return request({
    url: `/api/admin/knowledge/delete/${id}`,
    method: 'get'
  })
}
