import request from '@/utils/request_rest'

export function adminPermissionList(data) {
  return request({
    url: '/api/admin/permission/list',
    method: 'get',
    params: data
  })
}

export function adminPermissionAncestors(id) {
  return request({
    url: '/api/admin/permission/ancestors',
    method: 'get',
    params: {
      id
    }
  })
}

export function adminPermissionCheckAvailable(resource, id) {
  return request({
    url: '/api/admin/permission/checkAvailable',
    method: 'get',
    params: {
      resource,
      id
    }
  })
}

export function adminPermissionCreate(data) {
  return request({
    url: '/api/admin/permission/create',
    method: 'post',
    data
  })
}

export function adminPermissionUpdate(data) {
  return request({
    url: '/api/admin/permission/update',
    method: 'post',
    data
  })
}

export function adminPermissionDelete(id) {
  return request({
    url: '/api/admin/permission/delete',
    method: 'post',
    params: {
      id
    }
  })
}

export function adminRoleList() {
  return request({
    url: '/api/admin/role/list',
    method: 'get'
  })
}

export function adminRoleCheckAvailable(code, id) {
  return request({
    url: '/api/admin/role/checkAvailable',
    method: 'get',
    params: {
      code,
      id
    }
  })
}

export function adminRoleCreate(data) {
  return request({
    url: '/api/admin/role/create',
    method: 'post',
    data
  })
}

export function adminRoleUpdate(data) {
  return request({
    url: '/api/admin/role/update',
    method: 'post',
    data
  })
}

export function adminRoleDelete(id) {
  return request({
    url: '/api/admin/role/delete',
    method: 'post',
    params: {
      id
    }
  })
}

export function adminRoleCheckablePermissionList(id) {
  return request({
    url: '/api/admin/role/checkablePermissionList',
    method: 'get',
    params: {
      id
    }
  })
}

export function adminRoleCheckableAppList(id) {
  return request({
    url: '/api/admin/role/checkableAppList',
    method: 'get',
    params: {
      id
    }
  })
}

export function adminRoleAssignPermission(data) {
  return request({
    url: '/api/admin/role/assignPermission',
    method: 'post',
    data
  })
}

export function adminSysUserList(data) {
  return request({
    url: '/api/admin/user/list',
    method: 'get',
    params: data
  })
}

export function adminSysUserCheckAvailable(username, id) {
  return request({
    url: '/api/admin/user/checkAvailable',
    method: 'get',
    params: {
      username,
      id
    }
  })
}

export function adminSysUserCheckableDivisionList(id) {
  return request({
    url: '/api/admin/user/checkableDivisionList',
    method: 'get',
    params: {
      id
    }
  })
}

export function adminSysUserCreate(data) {
  return request({
    url: '/api/admin/user/create',
    method: 'post',
    data
  })
}

export function adminSysUserUpdate(data) {
  return request({
    url: '/api/admin/user/update',
    method: 'post',
    data
  })
}

export function adminSysUserResetPassword(data) {
  return request({
    url: '/api/admin/user/resetPassword',
    method: 'post',
    data
  })
}

export function adminSysUserDelete(id) {
  return request({
    url: '/api/admin/user/delete',
    method: 'post',
    params: {
      id
    }
  })
}

export function adminSysUserCheckableRoleList(id) {
  return request({
    url: '/api/admin/user/checkableRoleList',
    method: 'get',
    params: {
      id
    }
  })
}

export function adminSysUserAssignRole(data) {
  return request({
    url: '/api/admin/user/assignRole',
    method: 'post',
    data
  })
}

export function adminGetUserTypeRelList(id) {
  return request({
    url: '/api/admin/user/getUserTypeRel',
    method: 'get',
    params: {
      id
    }
  })
}
