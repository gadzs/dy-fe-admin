import request from '@/utils/request_rest'

export function loginLogList(data) {
  return request({
    url: '/api/admin/log/login/list',
    method: 'get',
    params: data
  })
}

export function operationLogList(data) {
  return request({
    url: '/api/admin/log/operation/list',
    method: 'get',
    params: data
  })
}

