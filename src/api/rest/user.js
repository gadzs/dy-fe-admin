import request from '@/utils/request_rest'

export function login(data) {
  return request({
    url: '/api/admin/login',
    method: 'post',
    data
  })
}

export function logout(data) {
  return request({
    url: '/api/admin/logout',
    method: 'post',
    data
  })
}

export function getInfo(sysComponent) {
  return request({
    url: '/api/admin/user/' + sysComponent + '/info',
    method: 'get'
  })
}

export function getApps() {
  return request({
    url: '/api/admin/user/apps',
    method: 'get'
  })
}

export function getPermissionList(sysComponent) {
  return request({
    url: '/api/admin/user/' + sysComponent + 'permission',
    method: 'get'
  })
}

export function modifyPassword(data) {
  return request({
    url: '/api/admin/user/modifyPassword',
    method: 'post',
    data
  })
}

export function verifyPassword(password) {
  return request({
    url: '/api/admin/user/verifyPassword',
    method: 'post',
    data: {
      password: password
    }
  })
}
