import request from '@/utils/request_rest'

export function adminAgenciesSearch(data) {
  return request({
    url: '/api/admin/agencies/search',
    method: 'get',
    params: data
  })
}

export function adminAgenciesCreate(data) {
  return request({
    url: '/api/admin/agencies/create',
    method: 'post',
    data
  })
}

export function adminAgenciesUpdate(data) {
  return request({
    url: '/api/admin/agencies/update',
    method: 'post',
    data
  })
}

export function adminAgenciesDelete(id) {
  return request({
    url: `/api/admin/agencies/delete/${id}`,
    method: 'get'
  })
}

export function adminAgenciesCheckAvailable(agenciesId, name) {
  return request({
    url: '/api/admin/agencies/checkAvailable',
    method: 'get',
    params: {
      name,
      agenciesId
    }
  })
}
export function adminAgenciesFindAll() {
  return request({
    url: '/api/admin/agencies/findAll',
    method: 'get'
  })
}
